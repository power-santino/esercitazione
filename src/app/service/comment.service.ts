import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comment } from '../models/comment';
import { PostService } from './post.service';
import { switchMap, filter, concatMap, groupBy, mergeMap, toArray, map } from 'rxjs/operators';
import { from } from 'rxjs';

@Injectable()
export class CommentService {

  constructor(private http: HttpClient, private mypostService: PostService ) { }
  getLastCommentFromIDpost(idPost: number){
      const apiUrl = "https://jsonplaceholder.typicode.com/comments?postId=" + idPost;
      return this.http.get<Comment[]>(apiUrl);
  }
  getPostFiltrati():Comment[] {
     let ret: Comment[] = [];
     // Faccio la chiamata
     this.mypostService.getAllPost().pipe(
      // Switch del singolo e qui parte un CICLO, un ciclo sui POST.
      switchMap(posts => from(posts)),
      // Da qui in poi lavoro sul singolo
      filter(singlePost => singlePost.id %2==0),
      /**  Faccio le chiamate con concatMap perchè
       *   A differenza di Switch non distrugge l'osservable precedente
       *   e fa partire le chiamate in parallelo.     
       */      
      concatMap((singlePost) => this.getLastCommentFromIDpost(singlePost.id)),
      // Come sopra divido in singoli commenti 
      switchMap((comment) => comment),
      // Inserisco in un solito contenitore titti gli ID
      groupBy((comment) => comment.postId),
      // Trasforma il gruppo in un array
      mergeMap(group => group.pipe(toArray())),
      // Prende solo l'ultimo commento 
      map(array => array[array.length - 1])
      // All'interno del subscribe ho i singoli commenti filtrati
      ).subscribe(comment =>{          
          //console.log(comment.body);
          ret.push(comment);       
      });
      return ret;
  }
}
