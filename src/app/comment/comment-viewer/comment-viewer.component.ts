import { Component, OnInit, Input } from '@angular/core';
import { Comment } from 'src/app/models/comment';

@Component({
  selector: 'app-comment-viewer',
  templateUrl: './comment-viewer.component.html',
  styleUrls: ['./comment-viewer.component.css']  
})
export class CommentViewerComponent implements OnInit {
  current = 'state2';
  constructor() { }
  @Input('commento') mycomment : Comment;
  ngOnInit() {    
  }
  ngOnDestroy(){
    this.current = 'state1';
  }
}
