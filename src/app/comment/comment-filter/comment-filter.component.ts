import { Component, OnInit } from '@angular/core';
import { Comment } from 'src/app/models/comment';
import { CommentService } from 'src/app/service/comment.service';
import { trigger, transition, query, style, stagger, animate } from '@angular/animations';


@Component({
  selector: 'app-comment-filter',
  templateUrl: './comment-filter.component.html',
  styleUrls: ['./comment-filter.component.css'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [ 
        query(':enter',style({opacity:0}),{optional: true}),
        query(':enter', [
          style({ opacity: 0 }),
          stagger('300ms', [
            animate('1s', style({ opacity: 1 }))
          ])
        ],{optional: true}),
        query(':leave', [
          stagger('300ms', [
            animate('0.7s', style({ opacity: 0 }))
          ])
        ], {optional: true})
        
      ])
    ])
  ]
})
export class CommentFilterComponent implements OnInit {
  constructor(private myCommentService: CommentService) { }
  ngOnInit() {
  }
  commentList: Comment[]=[];
  showComment(){
    this.commentList=this.myCommentService.getPostFiltrati();
  }
  removeComment(){
    this.commentList = [];
  }
}
