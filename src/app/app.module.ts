import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PostService } from './service/post.service';
import { HttpClientModule } from '@angular/common/http';
import { CommentService } from './service/comment.service';
import { CommentViewerComponent } from './comment/comment-viewer/comment-viewer.component';
import { CommentFilterComponent } from './comment/comment-filter/comment-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    CommentViewerComponent,
    CommentFilterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [PostService,CommentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
