import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']    
})
export class AppComponent {  
  title = 'Animazione commenti'; 
  // Esempio https://stackblitz.com/edit/angular-2h7q8l
  ngOnInit() {
  }
}
